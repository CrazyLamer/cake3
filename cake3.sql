-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               10.1.14-MariaDB - mariadb.org binary distribution
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица cake3.phinxlog
DROP TABLE IF EXISTS `phinxlog`;
CREATE TABLE IF NOT EXISTS `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake3.phinxlog: ~0 rows (приблизительно)
DELETE FROM `phinxlog`;
/*!40000 ALTER TABLE `phinxlog` DISABLE KEYS */;
INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`) VALUES
	(20161130181404, 'Initial', '2016-11-30 18:16:59', '2016-11-30 18:17:02');
/*!40000 ALTER TABLE `phinxlog` ENABLE KEYS */;


-- Дамп структуры для таблица cake3.statuses
DROP TABLE IF EXISTS `statuses`;
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake3.statuses: ~4 rows (приблизительно)
DELETE FROM `statuses`;
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`id`, `name`) VALUES
	(2, 'В работе'),
	(3, 'Выполнена'),
	(5, 'Отменена'),
	(1, 'Создана');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;


-- Дамп структуры для таблица cake3.tasks
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `comment` text NOT NULL,
  `type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `executor_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type_id`),
  KEY `author` (`user_id`),
  KEY `responsible_employee` (`executor_id`),
  KEY `status` (`status_id`),
  CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tasks_ibfk_3` FOREIGN KEY (`executor_id`) REFERENCES `users` (`id`),
  CONSTRAINT `tasks_ibfk_4` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake3.tasks: ~4 rows (приблизительно)
DELETE FROM `tasks`;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
INSERT INTO `tasks` (`id`, `name`, `description`, `comment`, `type_id`, `user_id`, `executor_id`, `status_id`, `created`, `modified`) VALUES
	(1, 'Написать парсер', 'Парсер маркет', 'Хз', 1, 1, 1, 1, '2016-11-28 12:05:44', '2016-11-28 12:05:44'),
	(2, 'Написать выгрузку', 'Написать выгрузку товаров', '', 1, 1, 1, 1, '2016-11-30 07:54:22', '2016-11-30 07:54:22'),
	(3, 'Меню', 'Добавить новый пункт меню', 'qwee', 2, 2, 1, 5, '2016-11-30 07:55:55', '2016-12-01 11:35:56'),
	(12, 'qewqeq', 'qweqweqweqwe', 'qweqweqweqweqwe', 3, 1, 4, 2, '2016-12-01 11:57:10', '2016-12-01 11:58:16'),
	(24, 'daesfsdrfdsf', 'sdfdsfdsfdsf', 'QQQQQQQQQ', 3, 1, 2, 2, '2016-12-01 12:15:18', '2016-12-01 13:58:10');
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;


-- Дамп структуры для таблица cake3.types
DROP TABLE IF EXISTS `types`;
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake3.types: ~3 rows (приблизительно)
DELETE FROM `types`;
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` (`id`, `name`) VALUES
	(3, 'Незначительное улучшение'),
	(2, 'Несрочный баг'),
	(1, 'Срочный баг');
/*!40000 ALTER TABLE `types` ENABLE KEYS */;


-- Дамп структуры для таблица cake3.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cake3.users: ~4 rows (приблизительно)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `description`, `role`, `created`, `modified`) VALUES
	(1, 'admin', '$2y$10$2mQVf/VLsYt5egejr/WUl.Cg1hvYUyEdjPeOlbXOX198PVsnZkubi', 'root', 2, '2016-11-28 13:40:30', '2016-11-30 16:23:37'),
	(2, 'superuser', '$2y$10$2mQVf/VLsYt5egejr/WUl.Cg1hvYUyEdjPeOlbXOX198PVsnZkubi', 'Главный программист', 1, '2016-11-28 13:40:30', '2016-12-01 13:51:53'),
	(3, 'user', '$2y$10$2mQVf/VLsYt5egejr/WUl.Cg1hvYUyEdjPeOlbXOX198PVsnZkubi', 'Программист', 1, '2016-11-28 13:40:30', '2016-11-28 13:40:30'),
	(4, 'user2', '$2y$10$2mQVf/VLsYt5egejr/WUl.Cg1hvYUyEdjPeOlbXOX198PVsnZkubi', 'Программист', 1, '2016-11-28 13:40:30', '2016-12-01 10:23:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
